
# Changelog for user-registration-hook

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.0.2] - 2020-11-13

Feature #20108, remove user deletion from ldap upon remove account (offloaded to keycloak)

## [v2.0.0] - 2017-11-29

Feature #10483: Provide user workspace drop API

Feature #10484: Remove user from LDAP upon D4Science user account 

## [v1.1.0] - 2016-07-30

Update for Liferay 6.2.5

## [v1.0.0] - 2015-06-30

First release 
