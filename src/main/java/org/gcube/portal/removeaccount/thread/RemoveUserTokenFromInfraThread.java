package org.gcube.portal.removeaccount.thread;
import static org.gcube.common.authorization.client.Constants.authorizationService;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class RemoveUserTokenFromInfraThread implements Runnable {
	private static Log _log = LogFactoryUtil.getLog(RemoveUserTokenFromInfraThread.class);
	private String username;
	/**
	 * 
	 * @param username
	 * @param scope
	 */
	public RemoveUserTokenFromInfraThread(String username) {
		super();
		this.username = username;
	}

	@Override
	public void run() {
		String infraContext = "/"+PortalContext.getConfiguration().getInfrastructureName();
		ScopeProvider.instance.set(infraContext);
		try {
			String userToken = authorizationService().resolveTokenByUserAndContext(username, infraContext);
			SecurityTokenProvider.instance.set(userToken);
			authorizationService().removeAllReleatedToken(username, infraContext);
			_log.info("*** Removed user token " + username + " in " + infraContext);
		} catch (Exception e) {
			_log.error("Could not remove user token " + username + " in " + infraContext, e);
		}
	}

}


