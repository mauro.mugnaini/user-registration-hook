package org.gcube.portal.removeaccount.thread;

import static org.gcube.common.authorization.client.Constants.authorizationService;

import java.util.List;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * @author Massimiliano Assante ISTI-CNR
 *
 */
public class RemoveUserFromJCR {
	private static Log _log = LogFactoryUtil.getLog(RemoveUserFromJCR.class);

	private String username2Delete;
	private String theAdminToken;
	private List<String> theAdminRolesString;


	public RemoveUserFromJCR(String userNameToDelete, String theAdminToken, List<String> theAdminRolesString) {
		super();
		this.username2Delete = userNameToDelete;
		this.theAdminRolesString = theAdminRolesString;
		this.theAdminToken = theAdminToken;
	}

	public boolean remove() {
		try {
			authorizationService().setTokenRoles(theAdminToken, theAdminRolesString);
			SecurityTokenProvider.instance.set(theAdminToken);
			_log.debug("Autorising drop workspace with infra manager token of " + theAdminToken);
			StorageHubClient shc = new StorageHubClient();
			_log.debug("BEFORE stohub.deleteUserAccount " + username2Delete);
			shc.deleteUserAccount(username2Delete);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			_log.error("Could not delete " + username2Delete + " from JCR  an error occurred on the service");
			return false;
		}
	}


}
