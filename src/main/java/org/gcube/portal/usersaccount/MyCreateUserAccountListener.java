package org.gcube.portal.usersaccount;

import java.util.ArrayList;
import java.util.List;

import org.gcube.common.portal.PortalContext;
import org.gcube.portal.notifications.thread.NewUserAccountNotificationThread;
import org.gcube.portal.removeaccount.thread.RemoveUserTokenFromInfraThread;
import org.gcube.portal.removeaccount.thread.RemovedUserAccountThread;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.gcube.vomanagement.usermanagement.model.GatewayRolesNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;
/**
 * 
 * @author Massimiliano Assante, CNR-ISTI
 *
 * Model Listeners are used to listen for events on models and do something in response. 
 * They’re similar in concept to custom action hooks, which perform actions in response to portal events (user login, for example). 
 * Model listeners implement the ModelListener interface.
 * 
 * @see https://dev.liferay.com/develop/tutorials/-/knowledge_base/6-2/creating-model-listeners
 */
public class MyCreateUserAccountListener extends BaseModelListener<User> {
	private static final Logger _log = LoggerFactory.getLogger(MyCreateUserAccountListener.class);
	final String SUBJECT = "New user account notification";
	private GroupManager gm;
//	private UserManager uMan;
	private RoleManager rm;

	@Override
	public void onAfterCreate(User user) throws ModelListenerException {
		_log.info("onAfterCreate NewUserAccount listener for: " + user.getScreenName() + " / " + user.getFullName());
		Thread WorkspaceAccountCreationThread = new Thread(new WorkspaceCreateAccountThread(user.getScreenName(), user.getFullName(), user.getEmailAddress()));
		WorkspaceAccountCreationThread.start();
		
		Thread emailManagersThread = new Thread(new NewUserAccountNotificationThread(user.getScreenName(), user.getFullName(), user.getEmailAddress()));
		emailManagersThread.start();
	}

	@Override
	public void onBeforeRemove(User user) throws ModelListenerException {
		gm = new LiferayGroupManager();
//		uMan = new LiferayUserManager();
		rm = new LiferayRoleManager();

		_log.info("onBeforeRemove userAccount listener for: " + user.getScreenName() + " / " + user.getFullName());
		String username2Delete = user.getScreenName();
		_log.info("Trying to remove user from JCR and not notify infra-managers ...");
		try {			
			_log.debug("Getting super user with role {}", GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
			//get the super user
			String infraContext = "/"+PortalContext.getConfiguration().getInfrastructureName();
			long rootgroupId = gm.getGroupIdFromInfrastructureScope(infraContext);			
			User theAdmin = LiferayUserManager.getRandomUserWithRole(rootgroupId, GatewayRolesNames.INFRASTRUCTURE_MANAGER);
			if (theAdmin == null) {
				System.out.println("Cannot add the user as VRE Folder admin: there is no user having role {} on context: " + GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
			}
			String adminUsername = theAdmin.getScreenName();
			_log.info("Got the super user: {}",adminUsername);
			String theAdminToken = PortalContext.getConfiguration().getCurrentUserToken(infraContext, adminUsername);
			List<String> rolesString = new ArrayList<String>();
			List<GCubeRole> theAdminRoles = rm.listRolesByUserAndGroup(theAdmin.getUserId(), rootgroupId);			
			for (GCubeRole gCubeRole : theAdminRoles) {
				rolesString.add(gCubeRole.getRoleName());
			}
			rolesString.add(GatewayRolesNames.INFRASTRUCTURE_MANAGER.getRoleName());
			Thread dropUserWorkspaceThread = new Thread(new RemovedUserAccountThread(username2Delete, theAdminToken, rolesString));
			dropUserWorkspaceThread.start();

			_log.info("Trying to remove user from Auth ...");
			Thread deleteAllUserAuthThread = new Thread(new RemoveUserTokenFromInfraThread(username2Delete));
			deleteAllUserAuthThread.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//	@Override
	//    public void onAfterRemove(User user)





}