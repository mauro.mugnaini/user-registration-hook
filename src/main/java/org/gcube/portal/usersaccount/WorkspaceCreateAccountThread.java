package org.gcube.portal.usersaccount;
import java.util.ArrayList;
import java.util.List;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import static org.gcube.common.authorization.client.Constants.authorizationService;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.vomanagement.usermanagement.RoleManager;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.gcube.vomanagement.usermanagement.model.GCubeRole;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkspaceCreateAccountThread implements Runnable {
	private static Logger _log = LoggerFactory.getLogger(WorkspaceCreateAccountThread.class);
	public static final String AUTORISED_INFRA_ROLE = "Infrastructure-Manager";
	private String newUserUserName;

	public WorkspaceCreateAccountThread(String newUserUserName,	String newUserFullName, String newUserEmailAddress) {
		super();
		this.newUserUserName = newUserUserName;
		_log.info("Calling StorageHub for Workspace creation to new user: " + newUserUserName + " ("+newUserFullName+")");
	}

	@Override
	public void run() {
		getWS(newUserUserName, "/"+PortalContext.getConfiguration().getInfrastructureName());
	}
	public static void getWS(String currentUsername, String context) {
		String username = currentUsername;
		String authorizationToken = PortalContext.getConfiguration().getCurrentUserToken(context, username);
		SecurityTokenProvider.instance.set(authorizationToken);
		ScopeProvider.instance.set(context);
		String previousToken = authorizationToken;
		try {					
			//get the super user
			String infraContext = "/"+PortalContext.getConfiguration().getInfrastructureName();
			long groupId = new LiferayGroupManager().getGroupIdFromInfrastructureScope(infraContext);
			RoleManager rm = new LiferayRoleManager();
			UserManager uMan = new LiferayUserManager();
			long roleId = rm.getRoleId(AUTORISED_INFRA_ROLE, groupId);
			List<GCubeUser> users = uMan.listUsersByGroupAndRole(groupId, roleId);
			if (users.isEmpty()) {
				_log.error("Cannot create this user account on storageHub there is no user having role {} on context {}",  AUTORISED_INFRA_ROLE , infraContext);
				return;
			}
			else {
				GCubeUser theAdmin = users.get(0);
				String theAdminToken = PortalContext.getConfiguration().getCurrentUserToken(infraContext, theAdmin.getUsername());
				List<GCubeRole> theAdminRoles = rm.listRolesByUserAndGroup(theAdmin.getUserId(), groupId);			
				List<String> rolesString = new ArrayList<String>();
				for (GCubeRole gCubeRole : theAdminRoles) {
					rolesString.add(gCubeRole.getRoleName());
				}
				authorizationService().setTokenRoles(theAdminToken, rolesString);
				SecurityTokenProvider.instance.set(theAdminToken);
				StorageHubClient shc = new StorageHubClient();
				shc.createUserAccount(currentUsername);
				SecurityTokenProvider.instance.set(previousToken);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			SecurityTokenProvider.instance.set(previousToken);
			return;
		}
	}
}


